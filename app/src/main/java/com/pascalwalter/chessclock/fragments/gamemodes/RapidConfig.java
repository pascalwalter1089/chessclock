package com.pascalwalter.chessclock.fragments.gamemodes;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.R;
import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;

import org.jetbrains.annotations.NotNull;

public class RapidConfig extends GameConfig {
    public RapidConfig() {
        super(R.layout.fragment_rapid_config);
    }

    @Override
    public void onViewCreated(
            @NonNull @NotNull View view,
            @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.viewModel = new ViewModelProvider(requireActivity()).get(GameConfigViewModel.class);
        view.findViewById(R.id.tenMinutesButton)
                .setOnClickListener(v -> setGameConfig(600, 0, "Schnellschach"));
        view.findViewById(R.id.thirtyMinutesButton)
                .setOnClickListener(v -> setGameConfig(1800, 0, "Schnellschach"));
        view.findViewById(R.id.fifetenPlusTenButton)
                .setOnClickListener(v -> setGameConfig(900, 10, "Schnellschach"));
        view.findViewById(R.id.twentyMinutesButton)
                .setOnClickListener(v -> setGameConfig(1200, 0, "Schnellschach"));
        view.findViewById(R.id.fourtyFivePlusFourtyFifeButton)
                .setOnClickListener(v -> setGameConfig(2700, 45, "Schnellschach"));
        view.findViewById(R.id.sixtyMinutesButton)
                .setOnClickListener(v -> setGameConfig(3600, 0, "Schnellschach"));
    }
}
