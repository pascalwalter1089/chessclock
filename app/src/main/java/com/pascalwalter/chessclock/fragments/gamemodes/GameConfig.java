package com.pascalwalter.chessclock.fragments.gamemodes;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.pascalwalter.chessclock.R;
import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;

public class GameConfig extends Fragment {
    protected GameConfigViewModel viewModel;

    public GameConfig(int layoutId) {
        super(layoutId);
    }

    protected void setGameConfig(int baseTime, int increment, String gameMode) {
        SharedPreferences sharedPref =
                getActivity()
                        .getSharedPreferences(
                                getString(R.string.shared_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.base_time), baseTime);
        editor.putInt(getString(R.string.increment_time), increment);
        editor.putString(getString(R.string.game_mode), gameMode);
        editor.apply();
        this.viewModel.setGameConfigSet();
    }

    protected void setGameConfigNotStart(int baseTime, int increment, String gameMode) {
        SharedPreferences sharedPref =
                getActivity()
                        .getSharedPreferences(
                                getString(R.string.shared_preferences), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.base_time), baseTime);
        editor.putInt(getString(R.string.increment_time), increment);
        editor.putString(getString(R.string.game_mode), gameMode);
        editor.apply();
    }

}
