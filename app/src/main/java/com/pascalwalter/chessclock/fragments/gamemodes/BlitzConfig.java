package com.pascalwalter.chessclock.fragments.gamemodes;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;
import com.pascalwalter.chessclock.R;

import org.jetbrains.annotations.NotNull;

public class BlitzConfig extends GameConfig {

    public BlitzConfig() {
        super(R.layout.fragment_blitz_config);
    }

    @Override
    public void onViewCreated(
            @NonNull @NotNull View view,
            @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.viewModel = new ViewModelProvider(requireActivity()).get(GameConfigViewModel.class);

        view.findViewById(R.id.threeMinuteButton)
                .setOnClickListener(v -> setGameConfig(180, 0, "Blitz"));
        view.findViewById(R.id.threePlusTwoButton)
                .setOnClickListener(v -> setGameConfig(180, 2, "Blitz"));
        view.findViewById(R.id.fiveMinutesButton)
                .setOnClickListener(v -> setGameConfig(300, 0, "Blitz"));
        view.findViewById(R.id.fivePlusFiveButton)
                .setOnClickListener(v -> setGameConfig(300, 5, "Blitz"));
    }
}
