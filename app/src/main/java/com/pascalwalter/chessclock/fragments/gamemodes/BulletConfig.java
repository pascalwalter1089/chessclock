package com.pascalwalter.chessclock.fragments.gamemodes;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.R;
import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;

public class BulletConfig extends GameConfig {
    public BulletConfig() {
        super(R.layout.fragment_bullet_config);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.viewModel = new ViewModelProvider(requireActivity()).get(GameConfigViewModel.class);
        view.findViewById(R.id.oneMinuteButton)
                .setOnClickListener(v -> setGameConfig(60, 0, "Bullet"));

        view.findViewById(R.id.onePlusOneButton)
                .setOnClickListener(v -> setGameConfig(60, 1, "Bullet"));

        view.findViewById(R.id.twoPlusOneButton)
                .setOnClickListener(v -> setGameConfig(120, 1, "Bullet"));
    }
}
