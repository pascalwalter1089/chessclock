package com.pascalwalter.chessclock.fragments.gamemodes;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.R;
import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;

public class CustomGameConfig extends GameConfig
{

    private NumberPicker baseTimePicker;
    private NumberPicker incrementTime;

    private int baseValue;
    private int incrementValue;
    public CustomGameConfig() {
        super(R.layout.fragment_custom_game_config);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        this.viewModel = new ViewModelProvider(requireActivity()).get(GameConfigViewModel.class);
        this.baseTimePicker = view.findViewById(R.id.numberpickerBaseTime);
        this.incrementTime = view.findViewById(R.id.numberpickerAdditionalTime);

        Button configure = view.findViewById(R.id.configureButton);

        configure.setOnClickListener(this::onConfigureClick);

        this.baseTimePicker.setMaxValue(120);
        this.baseTimePicker.setMinValue(1);
/*        this.baseTimePicker.setDisplayedValues(getDisplayValues(1, 120));*/
        this.baseTimePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                setGameSettings(
                        newVal * 60,
                        incrementTime.getValue()
                );
            }
        });

        this.incrementTime.setMaxValue(15);
        this.incrementTime.setMinValue(0);
       /* this.incrementTime.setDisplayedValues(getDisplayValues(0, 15));*/
        this.incrementTime.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                setGameSettings(
                        baseTimePicker.getValue() * 60,
                        newVal
                );
            }
        });

    }

    private void setGameSettings(int base, int increment) {
        setGameConfigNotStart(base, increment, "Custom Game");
    }

    private String[] getDisplayValues(int min, int max)
    {
        String[] values = new String[max + 1];

       for(int i = 0; i <= max ; i++) {
            values[i] = min + i + "";
        }

        return  values;
    }

    public void onConfigureClick(View view) {
        this.viewModel.setGameConfigSet();
    }
}
