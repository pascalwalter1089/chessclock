package com.pascalwalter.chessclock.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pascalwalter.chessclock.R;

import org.jetbrains.annotations.NotNull;

public class TimerFragment extends Fragment {
    private ListView hourListView;
    private ListView minuteListView;
    private ListView secondsListView;

    public TimerFragment() {
        super(R.layout.fragment_timer);
    }

    @Override
    public void onViewCreated(
            @NonNull @NotNull View view,
            @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Integer[] hoursData = new Integer[]{0, 1, 2, 3, 4, 5, 6};
        this.hourListView = view.findViewById(R.id.hoursListView);
        ArrayAdapter<Integer> hourListViewAdapter = new ArrayAdapter(
                getActivity().getBaseContext(),
                android.R.layout.simple_list_item_1,
                hoursData
        );

        this.hourListView.setSelection(20);

        Integer[] minutesAndSecondsData = new Integer[60];
        for (int i = 0; i < 60; i++)
        {
            minutesAndSecondsData[i] = i;
        }

        CircularListAdapter circularHoursAdapter = new CircularListAdapter(hourListViewAdapter);
        this.hourListView.setAdapter(circularHoursAdapter);

        this.minuteListView = view.findViewById(R.id.minutesListView);
        ArrayAdapter<Integer> minutesListAdapter = new ArrayAdapter(
                getActivity().getBaseContext(),
                android.R.layout.simple_list_item_1,
                minutesAndSecondsData
        );

        CircularListAdapter circularMinutesAndSecondsList =
                new CircularListAdapter(minutesListAdapter);
        this.minuteListView.setAdapter(circularMinutesAndSecondsList);
        this.minuteListView.setSelection(120);*/
    }
}
