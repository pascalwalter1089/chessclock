package com.pascalwalter.chessclock.fragments.game;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.R;
import com.pascalwalter.chessclock.viewmodel.GameViewModel;

public class InGameOptionsFragment extends Fragment {

    private TextView gameModeLabel;
    private GameViewModel gameViewModel;
    private Button pauseButton;

    public InGameOptionsFragment() {
        super(R.layout.fragment_ingame_options );
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        gameViewModel = new ViewModelProvider(requireActivity()).get(GameViewModel.class);

        gameModeLabel = view.findViewById(R.id.gameModeLabel);
        gameModeLabel.setText(gameViewModel.getGameMode());

        gameViewModel
                .getIsGamePausedLiveData()
                .observe(
                        getViewLifecycleOwner(),
                        e -> {
                            if (gameViewModel.getIsGamePaused()) {
                                pauseButton.setText(R.string.pause_button_continue);
                            } else {
                                pauseButton.setText(R.string.pause_game_button_pause);
                            }
                        });

        pauseButton = view.findViewById(R.id.pauseButton);
        pauseButton.setOnClickListener(
                e -> {
                    gameViewModel.setGamePaused(!gameViewModel.getIsGamePaused());
                });

        Button quitButton = view.findViewById(R.id.quitButton);
        quitButton.setOnClickListener(v -> gameViewModel.setIsGameQuited(true));
    }
}
