package com.pascalwalter.chessclock.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;

import com.pascalwalter.chessclock.R;

public class Utils
{

    @SuppressLint("DefaultLocale")
    public static String getTimeFromProgress(int progress) {
        return String.format(
                "%02d:%02d:%02d", progress / 3600, (progress % 3600) / 60, progress % 60);
    }

    public static String getStartButtonString(int baseTime, int increment) {
        baseTime = baseTime < 60 ? baseTime : baseTime / 60;
        increment = increment < 60 ? increment : increment / 60;
        if (0 == increment) {
            return String.format("%d Min", baseTime);
        }
        return String.format("%d | %d", baseTime, increment);
    }

    public static String getGameModeFromBaseTime(int baseTime, Context ctx) {
        if( baseTime < 180 ) {
            return ctx.getString(R.string.bullet);
        } else if (baseTime < 900 ) {
            return ctx.getString(R.string.blitz);
        } else {
            return ctx.getString(R.string.schnellschach);
        }
    }
}
