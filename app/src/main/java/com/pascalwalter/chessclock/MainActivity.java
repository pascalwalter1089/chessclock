package com.pascalwalter.chessclock;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import static com.pascalwalter.chessclock.utils.Utils.getGameModeFromBaseTime;
import static com.pascalwalter.chessclock.utils.Utils.getStartButtonString;

public class MainActivity extends AppCompatActivity {

    public static final String SELECTED_GAME_MODE = "SELECTED_GAME_MODE";
    public static final String BASE_TIME = "BASE_TIME";
    public static final String INCREMENT_TIME = "INCREMENT_TIME";

    private Button startButton;
    private Button gameModeChangeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.gameModeChangeButton = findViewById(R.id.gameModeCHangeButton);
        this.gameModeChangeButton.setOnClickListener(this::onGameModeChangeButtonClick);
        setGameModeChangeButtonText();

        this.startButton = findViewById(R.id.startButton);
        this.startButton.setOnClickListener(this::onStartButtonClick);
        this.startButton.setText(getString(R.string.start_game_button));
    }

    private void setGameModeChangeButtonText() {
        int baseTime = getPreferenceFromSharedPreference(getString(R.string.base_time), 180);
        int increment = getPreferenceFromSharedPreference(getString(R.string.increment_time), 0);
        setGameModeByBaseTime(baseTime);

        this.gameModeChangeButton.setText(getStartButtonString(baseTime, increment));
    }

    private void setGameModeByBaseTime(int baseTime) {
        SharedPreferences sharedPref =
                getSharedPreferences(getString(R.string.shared_preferences), Context.MODE_PRIVATE);
        String gameMode = getGameModeFromBaseTime(baseTime, this);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.game_mode), gameMode);
        editor.apply();
    }

    private int getPreferenceFromSharedPreference(String preferenceName, int defaultValue) {
        SharedPreferences sharedPref =
                getSharedPreferences(getString(R.string.shared_preferences), Context.MODE_PRIVATE);

        int preference = sharedPref.getInt(preferenceName, -1);
        if (preference == -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(preferenceName, defaultValue);
            editor.apply();
            preference = defaultValue;
        }

        return preference;
    }

    private void onGameModeChangeButtonClick(View v) {
        Intent intent = new Intent(MainActivity.this, GameModeSelectionActivity.class);
        startActivity(intent);
    }

    public void onStartButtonClick(View view) {
        SharedPreferences sharedPref =
                getSharedPreferences(getString(R.string.shared_preferences), Context.MODE_PRIVATE);
        Intent intent = new Intent(MainActivity.this, GameActivity.class);
        intent.putExtra(
                SELECTED_GAME_MODE, sharedPref.getString(getString(R.string.game_mode), "TEST"));
        intent.putExtra(BASE_TIME, sharedPref.getInt(getString(R.string.base_time), 0));
        intent.putExtra(INCREMENT_TIME, sharedPref.getInt(getString(R.string.increment_time), 0));

        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGameModeChangeButtonText();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
