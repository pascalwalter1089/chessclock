package com.pascalwalter.chessclock;

import static com.pascalwalter.chessclock.utils.Utils.getTimeFromProgress;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.viewmodel.GameViewModel;

import org.jetbrains.annotations.NotNull;

public class GameActivity extends AppCompatActivity {
    private Button playerOneButton;
    private CountDownTimer playerOneTimer;
    private CountDownTimer playerTwoTimer;

    private boolean timerPlayerOneRunning = false;
    private boolean timerPlayerTwoRunning = false;
    private Button playerTwoButton;

    private long timerPlayerOneRemainingTime;
    private long timerPlayerTwoRemainingTime;

    private GameViewModel gameViewModel;

    private int incrementTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Bundle extras = getIntent().getExtras();
        int baseTime = extras.getInt(MainActivity.BASE_TIME);
        incrementTime = extras.getInt(MainActivity.INCREMENT_TIME) * 1000;

        playerOneButton = findViewById(R.id.timeButtonPlayerOne);
        playerOneButton.setText(getTimeFromProgress(baseTime));
        playerTwoButton = findViewById(R.id.timeButtonPlayerTwo);
        playerTwoButton.setText(getTimeFromProgress(baseTime));

        timerPlayerOneRemainingTime = baseTime * 1000L;
        timerPlayerTwoRemainingTime = baseTime * 1000L;

        playerOneButton.setOnClickListener(this::togglePlayerTimers);
        playerTwoButton.setOnClickListener(this::togglePlayerTimers);

        playerOneTimer = getPlayerOneTimer();
        playerTwoTimer = getPlayerTwoTimer();

        gameViewModel = new ViewModelProvider(this).get(GameViewModel.class);
        gameViewModel.setGameMode(extras.getString(MainActivity.SELECTED_GAME_MODE));
        gameViewModel.setGamePaused(false);
        gameViewModel
                .getIsGamePausedLiveData()
                .observe(
                        this,
                        isGamePaused -> {
                            if (!isGamePaused) {
                                continueGame();
                            } else {
                                pauseGame();
                            }
                        });
        gameViewModel
                .getIsGameQuitedLiveData()
                .observe(
                        this,
                        isGameQuited -> {
                            if (isGameQuited) {
                                pauseGame();
                                new AlertDialog.Builder(this)
                                        .setTitle(R.string.quit_game_dialog_title)
                                        .setMessage(R.string.quit_game_dialog_message)
                                        .setPositiveButton(
                                                R.string.quit_game_dialog_positive,
                                                (dialog, which) -> {
                                                    dialog.dismiss();
                                                    quitGame();
                                                })
                                        .setNegativeButton(
                                                R.string.quit_game_dialog_negative,
                                                ((dialog, which) -> {
                                                    gameViewModel.setIsGameQuited(false);
                                                    continueGame();
                                                    dialog.dismiss();
                                                }))
                                        .create()
                                        .show();
                            }
                        });
    }

    private void pauseGame() {
        this.playerOneButton.setClickable(false);
        this.playerTwoButton.setClickable(false);
        this.playerTwoTimer.cancel();
        this.playerOneTimer.cancel();
    }

    private void continueGame() {
        this.playerOneButton.setClickable(true);
        this.playerTwoButton.setClickable(true);
        if (timerPlayerOneRunning) {
            timerPlayerOneRunning = false;
            timerPlayerTwoRunning = true;
            setUpTimerPlayerOne();
        } else if (timerPlayerTwoRunning) {
            timerPlayerOneRunning = true;
            timerPlayerTwoRunning = false;
            setUpTimerPlayerTwo();
        }
    }

    private void updateButtonText(Button btn, long remaining) {
        btn.setText(getTimeFromProgress((int) remaining / 1000));
    }

    private void togglePlayerTimers(@NotNull View view) {
        /*
         * When the button of player two gets clicked, the countdown for player one starts
         * If an increment exists the increment gets added to the players time who pushed the button.
         */
        if (view.getId() == R.id.timeButtonPlayerTwo) {
            togglePlayerButtons(this.playerTwoButton, this.playerOneButton);
            timerPlayerTwoRemainingTime += incrementTime;
            updateButtonText(playerTwoButton, timerPlayerTwoRemainingTime);
            setUpTimerPlayerOne();
        } else if (view.getId() == R.id.timeButtonPlayerOne) {
            togglePlayerButtons(this.playerOneButton, this.playerTwoButton);
            timerPlayerOneRemainingTime += incrementTime;
            updateButtonText(playerOneButton, timerPlayerOneRemainingTime);
            setUpTimerPlayerTwo();
        }
    }

    private void togglePlayerButtons(Button btn1, Button btn2) {
        btn1.setEnabled(false);
        btn2.setEnabled(true);
    }

    private void setUpTimerPlayerTwo() {
        if (!timerPlayerOneRunning && !timerPlayerTwoRunning) {
            timerPlayerTwoRunning = true;
            updateButtonColorOnPlayerButtonClicked(playerTwoButton, playerOneButton);
            startTimerPlayerTwo();
        } else if (timerPlayerOneRunning && !timerPlayerTwoRunning) {
            timerPlayerTwoRunning = true;
            timerPlayerOneRunning = false;
            updateButtonColorOnPlayerButtonClicked(playerTwoButton, playerOneButton);
            startTimerPlayerTwo();
        }
    }

    private void setUpTimerPlayerOne() {
        if (!timerPlayerOneRunning && !timerPlayerTwoRunning) {
            timerPlayerOneRunning = true;
            updateButtonColorOnPlayerButtonClicked(playerOneButton, playerTwoButton);
            startTimerPlayerOne();
        } else if (!timerPlayerOneRunning && timerPlayerTwoRunning) {
            timerPlayerTwoRunning = false;
            timerPlayerOneRunning = true;
            updateButtonColorOnPlayerButtonClicked(playerOneButton, playerTwoButton);
            startTimerPlayerOne();
        }
    }

    private void updateButtonColorOnPlayerButtonClicked(Button buttonOne, Button buttonTwo) {
        buttonOne.setBackgroundColor(getResources().getColor(R.color.secondary));
        buttonOne.setTextColor(getResources().getColor(R.color.primary));
        buttonTwo.setBackgroundColor(getResources().getColor(R.color.primary));
        buttonTwo.setTextColor(getResources().getColor(R.color.secondary));
    }

    private void startTimerPlayerOne() {
        playerTwoTimer.cancel();
        playerOneTimer = getPlayerOneTimer();
        playerOneTimer.start();
    }

    @NotNull
    private CountDownTimer getPlayerOneTimer() {
        return new CountDownTimer(timerPlayerOneRemainingTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerPlayerOneRemainingTime = millisUntilFinished;
                updateButtonText(playerOneButton, timerPlayerOneRemainingTime);
            }

            @Override
            public void onFinish() {
                playTimerUpSound();
            }
        };
    }

    private void playTimerUpSound() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.ringing);
        mp.setOnCompletionListener(MediaPlayer::release);
        mp.start();
    }

    private void startTimerPlayerTwo() {
        playerOneTimer.cancel();
        playerTwoTimer = getPlayerTwoTimer();
        playerTwoTimer.start();
    }

    @NotNull
    private CountDownTimer getPlayerTwoTimer() {
        return new CountDownTimer(timerPlayerTwoRemainingTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerPlayerTwoRemainingTime = millisUntilFinished;
                updateButtonText(playerTwoButton, timerPlayerTwoRemainingTime);
            }

            @Override
            public void onFinish() {
                gameOver();
            }
        };
    }

    private void gameOver() {
        playTimerUpSound();
        AlertDialog ad =
                new AlertDialog.Builder(this)
                        .setTitle("Game is over!")
                        .setMessage("The " + "Chess game is " + "over")
                        .setPositiveButton(
                                "Ok",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    quitGame();
                                })
                        .create();
        ad.show();
    }

    private void quitGame() {
        this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.gameViewModel.setGamePaused(true);
    }

    @Override
    public void onBackPressed() {
        this.gameViewModel.setIsGameQuited(true);
    }
}
