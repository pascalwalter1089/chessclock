package com.pascalwalter.chessclock;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.pascalwalter.chessclock.fragments.gamemodes.BlitzConfig;
import com.pascalwalter.chessclock.fragments.gamemodes.BulletConfig;
import com.pascalwalter.chessclock.fragments.gamemodes.CustomGameConfig;
import com.pascalwalter.chessclock.fragments.gamemodes.RapidConfig;
import com.pascalwalter.chessclock.viewmodel.GameConfigViewModel;

public class GameModeSelectionActivity extends AppCompatActivity {
    private GameConfigViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_mode_selection);

        this.viewModel = new ViewModelProvider(this).get(GameConfigViewModel.class);

        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.bulletConfigFragmentContainerView, BulletConfig.class, null)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.blitzConfigFragmentContainerView, BlitzConfig.class, null)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.rapidConfigFragmentContainerView, RapidConfig.class, null)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.customGameConfigView, CustomGameConfig.class, null)
                .commit();

        viewModel
                .getGameConfigSet()
                .observe(
                        this,
                        gameConfigSet -> {
                            onGameModeSelected(viewModel.isGameConfigSet());
                        });
    }

    public void onGameModeSelected(boolean configSet) {
        if (configSet) {
            Intent intent = new Intent(GameModeSelectionActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(GameModeSelectionActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
