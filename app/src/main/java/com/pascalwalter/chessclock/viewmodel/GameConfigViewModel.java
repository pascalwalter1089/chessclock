package com.pascalwalter.chessclock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GameConfigViewModel extends ViewModel {
    private final MutableLiveData<String> gameMode = new MutableLiveData<>();
    private final MutableLiveData<Integer> baseTime = new MutableLiveData<>();
    private final MutableLiveData<Integer> incrementTime = new MutableLiveData<>();
    private final MutableLiveData<Boolean> gameConfigSet = new MutableLiveData<>(false);

    public void setGameMode(String mode) {
        gameMode.setValue(mode);
    }

    public String getGameMode() {
        return gameMode.getValue();
    }

    public void setBaseTime(int baseTime) {
        this.baseTime.setValue(baseTime);
    }

    public Integer getBaseTime() {
        return baseTime.getValue();
    }

    public Integer getIncrementTime() {
        return incrementTime.getValue();
    }

    public void setIncrementTime(int incrementTime) {
        this.incrementTime.setValue(incrementTime);
    }

    public void setGameConfigSet() {
        this.gameConfigSet.setValue(true);
    }

    public boolean isGameConfigSet() {return this.gameConfigSet.getValue();}

    public MutableLiveData getGameConfigSet() {
        return this.gameConfigSet;
    }
}
