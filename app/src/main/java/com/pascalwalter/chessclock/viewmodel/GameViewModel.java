package com.pascalwalter.chessclock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GameViewModel extends ViewModel {
    private final MutableLiveData<String> gameMode = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isGamePaused = new MutableLiveData<Boolean>();
    private final MutableLiveData<Boolean> isGameQuited = new MutableLiveData<Boolean>(false);

    public String getGameMode() {
        return gameMode.getValue();
    }

    public void setGameMode(String gameMode) {
        this.gameMode.setValue(gameMode);
    }

    public void setGamePaused(boolean isGamePaused) {
        this.isGamePaused.setValue(isGamePaused);
    }

    public Boolean getIsGamePaused() {
        return this.isGamePaused.getValue();
    }

    public MutableLiveData<Boolean> getIsGamePausedLiveData() {
        return this.isGamePaused;
    }

    public void setIsGameQuited(Boolean val) {
        this.isGameQuited.setValue(val);
    }

    public MutableLiveData<Boolean> getIsGameQuitedLiveData() {return this.isGameQuited;}
}
